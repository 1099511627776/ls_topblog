<?php

class PluginTopblog_HookTopblog extends Hook {

    /*
     * ����������� ������� �� ����
	*/
    public function RegisterHook() {
        $this->AddHook('template_underTop', 'displayBlogs',__CLASS__);
    }
	
	public function displayBlogs($params){
		$sOrder='blog_rating';
		$sOrderWay='desc';
		$aFilter=array(
			'activate' => 1
		);
		$aResult=$this->Blog_GetBlogsRating(1,Config::Get('plugin.topblog.blog_count'));
		$aBlogs=$aResult['collection'];
		$iBlogCount = count($aBlogs);
		$i = 0;
		$aBlogs1 = array();
		foreach($aBlogs as $oBlog){
			$aBlogs1[] = array(
				'oBlog' => $oBlog,
				'position' => $iBlogCount - $i
			);
			$i++;
		}
//		print_r($aBlogs1);
		$this->Viewer_Assign('aBlogs',$aBlogs1);
        return $this->Viewer_Fetch(Plugin::GetTemplatePath('topblog').'top_blog.tpl');
	}
}
?>

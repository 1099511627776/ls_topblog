<ul class="topblog_container">
	<li class="topblog_item header">
		<strong>{$aLang.plugin.topblog.top_blogs} {$ls.plugin.topblog.config.blog_count}:</strong>
	</li>
{foreach from=$aBlogs item=oBlog}
<li class="topblog_item item_rating_{$oBlog['position']}">
	<a href="{$oBlog['oBlog']->getUrlFull()}" title="{$oBlog['oBlog']->getTitle()}">{$oBlog['oBlog']->getTitle()}</a>
</li>
{/foreach}
</ul>
